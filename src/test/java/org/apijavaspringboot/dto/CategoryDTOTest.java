package org.apijavaspringboot.dto;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CategoryDTOTest {

	@Test
    public void categoryDTOTest() {
        CategoryDTO categoryDTO = factoryCategoryDTO();
        
        assertNotNull(categoryDTO);
        assertEquals(categoryDTO.getCategory(), 123123);
        assertEquals(categoryDTO.getDescription(), "Categoria");
	}

    private CategoryDTO factoryCategoryDTO() {
        CategoryDTO categoryDTO = new CategoryDTO();
    	
    	categoryDTO.setCategory(123123);
        categoryDTO.setDescription("Categoria");
        
        return categoryDTO;
    }

}
