package org.apijavaspringboot.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.builder.impl.ProductBuilderImpl;
import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.CategoryDTO;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.resource.impl.ProductResourceImpl;
import org.apijavaspringboot.service.impl.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductResourceTest {

	@Mock
	private ProductBuilderImpl productBuilder;
	
	@Mock
    private ProductServiceImpl service;
	
	@InjectMocks
    private ProductResourceImpl resource;

	@Test
    public void findAllTest() throws GenericException {
		when(service.findAll()).thenReturn(new ProductCategoryOutputDomain());
        
		ProductCategoryOutputDomain productCategoryOutputDomain = resource.findAll();
        
        assertNotNull(productCategoryOutputDomain);
    }

    @Test
    public void findByIdTest() throws GenericException {
    	when(service.findById(1001)).thenReturn(new ProductDTO());
    	
    	ProductDTO productDTO = resource.findById(1001);

    	assertNotNull(productDTO);
    }

    @Test
    public void uploadTest() throws GenericException {
    	ProductResourceImpl productResourceImpl = mock(ProductResourceImpl.class);
    	
    	MockMultipartFile file = new MockMultipartFile(
    			"file",
    			"planilha.xlsx",
    			MediaType.TEXT_PLAIN,
    			"Dados planilha".getBytes());
    	
    	doNothing().when(productResourceImpl).upload(isA(MultipartFile.class));
    	productResourceImpl.upload(file);
     
        verify(productResourceImpl, times(1)).upload(file);
    }

    @Test
    public void statusTest() throws GenericException {
    	when(service.status(1)).thenReturn("Seccessful");
    	
    	String status = resource.status(1);

    	assertNotNull(status);
    	assertEquals(status, "Seccessful");
    }

    @Test
    public void updateTest() throws GenericException {
    	ProductDTO productDTOMock = factoryProductDTO();
    	
    	when(service.update(productDTOMock)).thenReturn(productDTOMock);
    	
    	ProductDTO productDTO = resource.update(productDTOMock);

    	assertNotNull(productDTO);
    }

    @Test
    public void deleteTest() throws GenericException {
    	when(service.delete(1001)).thenReturn(true);
    	
    	boolean isDeletedProductDTO = resource.delete(1001);

    	assertEquals(isDeletedProductDTO, true);
    }

    private ProductDTO factoryProductDTO() {
    	ProductDTO productDTO = new ProductDTO();
    	CategoryDTO categoryDTO = new CategoryDTO();
    	
    	categoryDTO.setCategory(123123);
    	categoryDTO.setDescription("Categoria");
    	
    	productDTO.setCategory(categoryDTO);
    	productDTO.setId(1001);
    	productDTO.setName("Furadeira");
    	productDTO.setFreeShipping(true);
    	productDTO.setDescription("Furadeira eficiente");
    	productDTO.setPrice(100);
    	
    	return productDTO;
    }
}
