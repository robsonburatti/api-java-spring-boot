package org.apijavaspringboot.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "category")
public class CategoryDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Unique category identifier")
	@Id
	@Column(name = "category", unique = true, nullable = false)
	@Getter
	@Setter
	private long category;
	
	@ApiModelProperty(notes = "Category description")
	@Column(name = "description", nullable = false)
	@Getter
	@Setter
	private String description;

}
