package org.apijavaspringboot.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class ProductDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes =  "Unique product identification code")
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@Getter
	@Setter 
	private long id;
    
	@ApiModelProperty(notes =  "Name of the product")
	@Column(name = "name", nullable = false)
	@Getter
	@Setter
	private String name;
    
	@ApiModelProperty(notes = "Identifier if shipping is free")
	@Column(name = "free_shipping", nullable = false)
	@Getter
	@Setter
	private boolean freeShipping;
    
	@ApiModelProperty(notes = "Brief product description")
	@Column(name = "description", nullable = false)
	@Getter
	@Setter
	private String description;
    
	@ApiModelProperty(notes = "Price of the product")
	@Column(name = "price", nullable = false)
	@Getter
	@Setter
	private double price;
	
	@ApiModelProperty(notes = "Category to which the product belongs")
	@ManyToOne
	@JoinColumn(name = "category", nullable = false)
	@Getter @Setter private CategoryDTO category;

}
