package org.apijavaspringboot.builder.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.builder.ProductBuilder;
import org.apijavaspringboot.domain.ProductCategoryInputDomain;
import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.exceptions.InternalServerErrorException;
import org.apijavaspringboot.exceptions.NoContentException;
import org.apijavaspringboot.exceptions.ResponseStatusCodes;
import org.apijavaspringboot.factory.ProductFactory;
import org.apijavaspringboot.repository.CategoryRepository;
import org.apijavaspringboot.repository.ProductRepository;

@Service
public class ProductBuilderImpl implements ProductBuilder {

	@Autowired
	private ProductFactory productFactory;
	
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public ProductCategoryOutputDomain findAll() throws GenericException {
		try {
			ProductCategoryOutputDomain productCategoryOutputDomain = new ProductCategoryOutputDomain();
			Iterator<ProductDTO> iteratorProducts = productRepository.findAll().iterator();
			List<ProductDTO> products = new ArrayList<ProductDTO>();
			
			productCategoryOutputDomain.setProducts(products);
	
			while (iteratorProducts.hasNext()) {
				ProductDTO productDTO = iteratorProducts.next();
	
				products.add(productDTO);
			}
	
			return productCategoryOutputDomain;
		} catch (Exception exception) {
			throw new InternalServerErrorException(ResponseStatusCodes.INTERNAL_SERVER_ERROR_500.getMessage(), exception.getCause());
		}
	}

	@Override
	public ProductDTO findById(long id) throws GenericException {
		boolean hasProduct = productRepository.existsById(id);
		
		if (!hasProduct) {
			throw new NoContentException(ResponseStatusCodes.NO_CONTENT_204.getMessage());
		}
		
		return productRepository.findById(id).get();
	}

	@Override
	public String upload(@Payload MultipartFile file) throws GenericException {
		try {
			ProductCategoryInputDomain productCategoryInputDomain = productFactory.processingFile(file);
			Long category = productCategoryInputDomain.getCategory().getCategory();
			boolean hasCategory = categoryRepository.existsById(category);
			
			if (!hasCategory) {
				categoryRepository.save(productCategoryInputDomain.getCategory());
			}
			
			for (ProductDTO product : productCategoryInputDomain.getProducts()) {
				boolean hasProduct = productRepository.existsById(product.getId());
				
				if (hasProduct) {
					continue;
				} else {
					productRepository.save(product);
				}
			}
			
//			try {
//				Thread.sleep(120000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			return "Successful";
		} catch (Exception exception) {
			// TODO: handle exception
			throw new InternalServerErrorException(ResponseStatusCodes.INTERNAL_SERVER_ERROR_500.getMessage(), exception.getCause());
		}
	}

	@Override
	public String status(long idPlan) throws GenericException {
		// TODO Auto-generated method stub
		return "Successful";
	}

	@Override
	public ProductDTO update(ProductDTO product) throws GenericException {
		boolean hasProduct = productRepository.existsById(product.getId());
		
		if (!hasProduct) {
			throw new NoContentException(ResponseStatusCodes.NO_CONTENT_204.getMessage());
		}
		
		return productRepository.save(product);
	}

	@Override
	public boolean delete(long id) throws GenericException {
		boolean hasProduct = productRepository.existsById(id);
		
		if (!hasProduct) {
			throw new NoContentException(ResponseStatusCodes.NO_CONTENT_204.getMessage());
		}
		
		productRepository.delete(productRepository.findById(id).get());
		
		boolean isDeleted = !productRepository.existsById(id);
		
		return isDeleted;
	}

}
