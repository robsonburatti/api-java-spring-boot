package org.apijavaspringboot.service.impl;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.builder.ProductBuilder;
import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.BadRequestException;
import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.exceptions.ResponseStatusCodes;
import org.apijavaspringboot.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Queue queue;
    
	@Autowired
    private ProductBuilder productBuilder;

	@Override
	public ProductCategoryOutputDomain findAll() throws GenericException {
		// TODO Auto-generated method stub
		return productBuilder.findAll();
	}

	@Override
	public ProductDTO findById(long id) throws GenericException {
		// TODO Auto-generated method stub
		if (id <= 0) {
			throw new BadRequestException(ResponseStatusCodes.BAD_REQUEST_400.getMessage());
		}
		
		return productBuilder.findById(id);
	}

	@Override
	public void upload(MultipartFile file) throws GenericException {
		// TODO Auto-generated method stub
		rabbitTemplate.convertAndSend(queue.getName(), productBuilder.upload(file));
	}

	@Override
	public String status(long idPlan) throws GenericException {
		// TODO Auto-generated method stub
		if (idPlan <= 0) {
			throw new BadRequestException(ResponseStatusCodes.BAD_REQUEST_400.getMessage());
		}
		
		return productBuilder.status(idPlan);
	}

	@Override
	public ProductDTO update(ProductDTO product) throws GenericException {
		// TODO Auto-generated method stub
		if (product == null) {
			throw new BadRequestException(ResponseStatusCodes.BAD_REQUEST_400.getMessage());
		}
		
		return productBuilder.update(product);
	}

	@Override
	public boolean delete(long id) throws GenericException {
		// TODO Auto-generated method stub
		if (id <= 0) {
			throw new BadRequestException(ResponseStatusCodes.BAD_REQUEST_400.getMessage());
		}
		
		return productBuilder.delete(id);
	}
	
}
