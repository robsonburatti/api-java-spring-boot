package org.apijavaspringboot.service;

import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;

public interface ProductService {
	
	ProductCategoryOutputDomain findAll() throws GenericException;
	
	ProductDTO findById(long id) throws GenericException;
	
	void upload(MultipartFile file) throws GenericException;

	String status(long idPlan) throws GenericException;
	
	ProductDTO update(ProductDTO product) throws GenericException;
	
	boolean delete(long id) throws GenericException;

}
