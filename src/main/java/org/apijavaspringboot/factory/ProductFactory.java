package org.apijavaspringboot.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.domain.ProductCategoryInputDomain;
import org.apijavaspringboot.dto.CategoryDTO;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.exceptions.InternalServerErrorException;
import org.apijavaspringboot.exceptions.ResponseStatusCodes;
import org.apijavaspringboot.utils.MessagesUtils;
import org.apijavaspringboot.utils.WorkbookUtils;

@Component
public class ProductFactory {
	
	private static final int LINE_CATEGORY = 0;
	private static final int LINE_START_PRODUCTS = 3;
	private static final String DIRECTORY = "products";
	
	public ProductCategoryInputDomain processingFile(MultipartFile file) throws GenericException {
		try {
			WorkbookUtils workbookUtils = new WorkbookUtils(file, DIRECTORY);
			
			ProductCategoryInputDomain productCategoryInputDomain = new ProductCategoryInputDomain();
			CategoryDTO categoryDTO = new CategoryDTO();
			List<ProductDTO> products = new ArrayList<ProductDTO>();
			productCategoryInputDomain.setProducts(products);
			
			while (workbookUtils.rowIterator.hasNext()) {
				Row row = workbookUtils.rowIterator.next();

				if (row.getRowNum() == LINE_CATEGORY) {
					Map<Integer, Object> mapCategory = workbookUtils.loadRow(row);
					
					categoryDTO.setDescription(mapCategory.get(0).toString());
					categoryDTO.setCategory(Long.parseLong(String.valueOf(mapCategory.get(1))));
					
					productCategoryInputDomain.setCategory(categoryDTO);
				}
				
				if (row.getRowNum() >= LINE_START_PRODUCTS) {
					Map<Integer, Object> mapProduct = workbookUtils.loadRow(row);
					ProductDTO productDTO = new ProductDTO();
					
					productDTO.setId(Long.parseLong(String.valueOf(mapProduct.get(0))));
					productDTO.setName(mapProduct.get(1).toString());
					productDTO.setFreeShipping(String.valueOf(mapProduct.get(2)).equalsIgnoreCase("1"));
					productDTO.setDescription(mapProduct.get(3).toString());
					productDTO.setPrice(Double.parseDouble(String.valueOf(mapProduct.get(4))));
					productDTO.setCategory(categoryDTO);
					
					products.add(productDTO);
				}
			}

			return productCategoryInputDomain;
		} catch (Exception exception) {
			throw new InternalServerErrorException(
					ResponseStatusCodes.INTERNAL_SERVER_ERROR_500.getMessage(), 
					new Throwable(MessagesUtils.ERROR_PROCESSING_FILE.getMessage(), exception.getCause()));
		}
	}

}
