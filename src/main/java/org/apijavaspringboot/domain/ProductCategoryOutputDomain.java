package org.apijavaspringboot.domain;

import java.util.List;

import org.apijavaspringboot.dto.ProductDTO;

public class ProductCategoryOutputDomain {

	private List<ProductDTO> products;
    
	public List<ProductDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}
	
}
