package org.apijavaspringboot.repository;

import org.springframework.data.repository.CrudRepository;

import org.apijavaspringboot.dto.ProductDTO;

public interface ProductRepository extends CrudRepository<ProductDTO, Long> {


}
