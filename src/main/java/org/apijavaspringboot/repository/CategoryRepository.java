package org.apijavaspringboot.repository;

import org.springframework.data.repository.CrudRepository;

import org.apijavaspringboot.dto.CategoryDTO;

public interface CategoryRepository extends CrudRepository<CategoryDTO, Long> {

	
}
