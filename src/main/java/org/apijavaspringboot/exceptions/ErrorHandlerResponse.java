package org.apijavaspringboot.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorHandlerResponse extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoContentException.class)
    public ResponseEntity<Object> noContentExceptionHandler(GenericException exception, WebRequest request) {
    	return handleExceptionInternal(exception, new ErrorResponse().addError(exception.getError()), new HttpHeaders(), HttpStatus.OK, request);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Object> badRequestExceptionHandler(GenericException exception, WebRequest request) {
    	return handleExceptionInternal(exception, new ErrorResponse().addError(exception.getError()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request); 
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<Object> internalServerErrorExceptionHandler(GenericException exception, WebRequest request) {
    	return handleExceptionInternal(exception, new ErrorResponse().addError(exception.getError()), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> notFoundExceptionHandler(GenericException exception, WebRequest request) {
        return handleExceptionInternal(exception, new ErrorResponse().addError(exception.getError()), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}
