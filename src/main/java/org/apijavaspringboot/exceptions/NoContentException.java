package org.apijavaspringboot.exceptions;

public class NoContentException extends GenericException {
	
	private static final long serialVersionUID = 1L;
	public static final ResponseStatusCodes ERROR = ResponseStatusCodes.NO_CONTENT_204;

    public NoContentException() {
        super(new Error(ERROR.getStatusCode(), ERROR.getMessage(), ERROR.getCause()));
    }

    public NoContentException(String message) {
        super(message, new Error(ERROR.getStatusCode(), message, ERROR.getCause()));
    }

    public NoContentException(String message, Throwable cause) {
        super(message, cause, new Error(ERROR.getStatusCode(), message, ((cause == null) ? ERROR.getCause() : cause.getMessage())));
    }

    public NoContentException(Throwable cause) {
        super(cause, new Error(ERROR.getStatusCode(), ERROR.getMessage(), ((cause == null) ? ERROR.getCause() : cause.getMessage())));
    }

}
