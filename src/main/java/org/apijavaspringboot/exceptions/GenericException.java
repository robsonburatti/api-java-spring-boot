package org.apijavaspringboot.exceptions;

public class GenericException extends Exception {

	private static final long serialVersionUID = 1L;
	private final transient Error error;

	public GenericException(Error error) {
        super();
        
        this.error = error;
    }
	
	public GenericException(String message, Error error) {
        super(message);
        error.setMessage(message);
        this.error = error;
    }
	
	public GenericException(String message, Throwable cause, Error error) {
        super(message, cause);
        error.setMessage(message);
        error.setCause(((cause == null) ? error.getCause() : cause.getMessage()));
        this.error = error;
    }

    public GenericException(Throwable cause, Error error) {
        super(cause);
        error.setCause(((cause == null) ? error.getCause() : cause.getMessage()));
        this.error = error;
    }

    public GenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Error error) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setMessage(message);
        error.setCause(((cause == null) ? error.getCause() : cause.getMessage()));
        this.error = error;
    }

	public Error getError() {
		return error;
	}

}
