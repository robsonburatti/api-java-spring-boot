package org.apijavaspringboot.exceptions;

public class BadRequestException extends GenericException {

	private static final long serialVersionUID = 1L;
	public static final ResponseStatusCodes ERROR = ResponseStatusCodes.BAD_REQUEST_400;

    public BadRequestException() {
        super(new Error(ERROR.getStatusCode(), ERROR.getMessage(), ERROR.getCause()));
    }

    public BadRequestException(String message) {
        super(message, new Error(ERROR.getStatusCode(), message, ERROR.getCause()));
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause, new Error(ERROR.getStatusCode(), message, cause.getMessage()));
    }

    public BadRequestException(Throwable cause) {
        super(cause, new Error(ERROR.getStatusCode(), ERROR.getMessage(), cause.getMessage()));
    }
}
