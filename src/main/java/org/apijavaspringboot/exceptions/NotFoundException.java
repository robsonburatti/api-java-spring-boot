package org.apijavaspringboot.exceptions;

public class NotFoundException extends GenericException {

	private static final long serialVersionUID = 1L;
	public static final ResponseStatusCodes ERROR = ResponseStatusCodes.NOT_FOUND_404;

    public NotFoundException() {
        super(new Error(ERROR.getStatusCode(), ERROR.getMessage(), ERROR.getMessage()));
    }

    public NotFoundException(String message) {
        super(message, new Error(ERROR.getStatusCode(), message, ERROR.getCause()));
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause, new Error(ERROR.getStatusCode(), message, ((cause == null) ? ERROR.getCause() : cause.getMessage())));
    }

    public NotFoundException(Throwable cause) {
        super(cause, new Error(ERROR.getStatusCode(), ERROR.getMessage(), ((cause == null) ? ERROR.getCause() : cause.getMessage())));
    }

}
