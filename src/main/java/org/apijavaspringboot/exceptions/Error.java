package org.apijavaspringboot.exceptions;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({
	"statusCode",
	"message"
})
public class Error {
	
	@ApiModelProperty(notes = "Request return status code")
	private Integer statusCode;
	
	@ApiModelProperty(notes = "Description of the error message that occurred")
    private String message;
	
	@ApiModelProperty(notes = "Technical description of the cause of the error that occurred")
    private String cause;

}
