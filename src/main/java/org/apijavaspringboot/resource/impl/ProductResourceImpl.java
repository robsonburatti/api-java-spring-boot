package org.apijavaspringboot.resource.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.resource.ProductResource;
import org.apijavaspringboot.service.ProductService;

@CrossOrigin("*")
@RestController
public class ProductResourceImpl implements ProductResource {

	@Autowired
    private ProductService productService;

	@Override
	public ProductCategoryOutputDomain findAll() throws GenericException {
		// TODO Auto-generated method stub
		return productService.findAll();
	}

	@Override
	public ProductDTO findById(long id) throws GenericException {
		// TODO Auto-generated method stub
		return productService.findById(id);
	}

	@Override
	public void upload(MultipartFile file) throws GenericException {
		// TODO Auto-generated method stub
		productService.upload(file);
	}

	@Override
	public String status(long idPlan) throws GenericException {
		// TODO Auto-generated method stub
		return productService.status(idPlan);
	}

	@Override
	public ProductDTO update(ProductDTO product) throws GenericException {
		// TODO Auto-generated method stub
		return productService.update(product);
	}

	@Override
	public boolean delete(long id) throws GenericException {
		// TODO Auto-generated method stub
		return productService.delete(id);
	}

}
