package org.apijavaspringboot.utils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.exceptions.InternalServerErrorException;
import org.apijavaspringboot.exceptions.ResponseStatusCodes;

public class WorkbookUtils {
	
	private XSSFWorkbook xssfWorkbook;
	private XSSFSheet sheetProducts;
	private static final String PROPERTY_TMPDIR = "java.io.tmpdir";
	public Iterator<Row> rowIterator;
	
	public WorkbookUtils(MultipartFile file, String folder) throws GenericException {
		try {
			String tmpdir = System.getProperty(PROPERTY_TMPDIR);
			
	        Path newFolder = Paths.get(tmpdir + File.separator + folder + File.separator);
			Path filePath = newFolder.resolve(file.getOriginalFilename());
			
	        Files.createDirectories(newFolder);
			file.transferTo(filePath.toFile());
			
			FileInputStream fileInputStream = new FileInputStream(new File(filePath.toFile().getAbsolutePath()));

			xssfWorkbook = new XSSFWorkbook(fileInputStream);
			sheetProducts = xssfWorkbook.getSheetAt(0);

			rowIterator = sheetProducts.iterator();
		} catch (Exception exception) {
			throw new InternalServerErrorException(
					ResponseStatusCodes.INTERNAL_SERVER_ERROR_500.getMessage(), 
					new Throwable(MessagesUtils.ERROR_CREATE_DIRECTORY_TEMPORARY.getMessage(), exception.getCause()));
		}
	}
	
	public Map<Integer, Object> loadRow(Row row) throws GenericException {
		try {
			Iterator<Cell> cellIterator = row.cellIterator();
			Map<Integer, Object> toReturn = new HashMap<Integer, Object>();
			
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				
				switch (cell.getCellType()) {
					case STRING:
						toReturn.put(cell.getColumnIndex(), cell.getStringCellValue());
						break; 
					case NUMERIC:
						if (cell.getNumericCellValue() == (long) cell.getNumericCellValue()) {
							toReturn.put(cell.getColumnIndex(), (long) cell.getNumericCellValue());
						} else {
							toReturn.put(cell.getColumnIndex(), cell.getNumericCellValue());
						}
						break;
					case BOOLEAN:
						toReturn.put(cell.getColumnIndex(), cell.getBooleanCellValue());
						break;
					default :
				}
			}
			
			return toReturn;
		} catch (Exception exception) {
			throw new InternalServerErrorException(
					ResponseStatusCodes.INTERNAL_SERVER_ERROR_500.getMessage(), 
					new Throwable(MessagesUtils.ERROR_PROCESSING_READING_LINE_FILE.getMessage(), exception.getCause()));
		}
	}

}
