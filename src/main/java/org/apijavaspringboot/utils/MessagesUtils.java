package org.apijavaspringboot.utils;

public enum MessagesUtils {
	
	STATUS_CODE_NO_CONTENT_204("Resource sought does not exist"),
    STATUS_CODE_BAD_REQUEST_400("Required parameters not reported"),
    STATUS_CODE_INTERNAL_SERVER_ERROR_500("Request could not be processed"),
    STATUS_CODE_NOT_FOUND_404("The server cannot find the requested resource"),
	
    ERROR_PROCESSING_FILE("Error processing file"),
	ERROR_CREATE_DIRECTORY_TEMPORARY("There was an error creating the temporary directory for processing the file"),
	ERROR_PROCESSING_READING_LINE_FILE("An error occurred while reading a line from the file");

	private String message;
	
	MessagesUtils(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
