package org.apijavaspringboot;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan("org.apijavaspringboot.*")
@SpringBootApplication
public class ApiJavaSpringBootApplication {

	@Value("${queue.upload.status}")
	private String uploadQueue;
	
	public static void main(String[] args) {
		SpringApplication.run(ApiJavaSpringBootApplication.class, args);
	}

	@Bean
    public Queue queue() {
        return new Queue(uploadQueue, true);
    }

}
