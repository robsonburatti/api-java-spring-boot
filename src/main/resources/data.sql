DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS category;

----------------------------------------------------------------

CREATE TABLE category (
    category BIGINT NOT NULL,
    description VARCHAR(250) NOT NULL,
    CONSTRAINT category_pk PRIMARY KEY (category)
);

----------------------------------------------------------------

DROP TABLE IF EXISTS product;

CREATE TABLE product (
    id BIGINT NOT NULL,
    name VARCHAR(250) NOT NULL,
    free_shipping BOOLEAN NOT NULL,
    description VARCHAR(250) NOT NULL,
    price NUMERIC(10,4) NOT NULL,
    category BIGINT NOT NULL,
    CONSTRAINT product_pk PRIMARY KEY (id)
);


ALTER TABLE product ADD CONSTRAINT category_product_fk
FOREIGN KEY (category)
REFERENCES category (category)
ON DELETE NO ACTION
ON UPDATE NO ACTION;